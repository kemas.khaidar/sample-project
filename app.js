const express = require('express');

// Setup Application
require('dotenv').config();
const bodyParser = require('body-parser');
const app = express();
const authJwt = require('./helpers/jwt');
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
)
app.use(authJwt());

// Run application at server port 5000
const port = 5000;
app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})

// Route integration
const userRoutes = require('./routes/user');
const authRoutes = require('./routes/auth');
app.use(`/user`, userRoutes);
app.use(`/auth`, authRoutes);

// Connect to postgresql database
const pool = require('./helpers/connection');
pool.connect(error => {
  if (error) {
    console.log(error);
  } else {
    console.log('Database Connected');
  }
});

module.exports = app;