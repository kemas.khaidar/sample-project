'use strict';
const express = require('express');
const { login } = require('../controllers/auth/index');
const { addUser } = require('../controllers/user/index');

const router = express.Router();

router.post('/login', login);
router.post('/register', addUser);

module.exports = router;
