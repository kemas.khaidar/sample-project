'use strict';
const express = require('express');
const { 
  listUser, 
  addUser,
  removeUser
} = require('../controllers/user/index');

const router = express.Router();

router.get('/', listUser);
router.post('/', addUser);
router.delete('/', removeUser);

module.exports = router;
