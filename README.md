# sampe-project

## Installation

This is a [Node.js](https://nodejs.org/en/) module available through the
[npm registry](https://www.npmjs.com/).

Before installing, [download and install Node.js](https://nodejs.org/en/download/).
Node.js 0.10 or higher is required.

Installation is done using the
[`npm install` command](https://docs.npmjs.com/getting-started/installing-npm-packages-locally):

```console
$ npm install
```

## Features

  * Register New User
  * Login
  * List User with Filter and pagination
  * Add User (After login with existing user)
  * Delete User


## Quick Start

  This section guide how to start the project.
  You need to set .env to your database connection. Example:

```js
TOKEN_SECRET={YourToken}
DB_USERNAME=kindrakusuma
DB_HOST=localhost
DB_DATABASE=postgres
DB_PORT=5432
```

  Then you need to execute sql statement on migration.sql.
  Start the server:

```console
$ npm start
```

  View the website at: http://localhost:5000

### Running Tests

To run the test suite, first install the dependencies, then run `npm test`:

```console
$ npm install
$ npm test
```
To run with coverage result:

```console
$ npm test -- --coverage
```

Current test Coverage:
![coverage](https://gitlab.com/kemas.khaidar/sample-project/-/raw/master/testResult.jpg)


## People

The original author of this project is Kemas Khaidar Ali Indrakusuma