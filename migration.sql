create table if not exists sample_user (
	id serial primary key,
	username varchar(50) unique not null,
	password varchar(255) not null,
	email varchar(255) unique not null,
	created_at timestamp not null,
	last_updated_at timestamp not null,
	last_login timestamp,
	is_active boolean
);