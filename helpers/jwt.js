'use strict';

const expressJwt = require('express-jwt').expressjwt;

function authJwt() {
  const secret = process.env.TOKEN_SECRET;

  return expressJwt({
    secret,
    algorithms: ['HS256']
  }).unless({
    path: [
      `/auth/login`,
      `/auth/register`
    ],
  });
}

module.exports = authJwt;
