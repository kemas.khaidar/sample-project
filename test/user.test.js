const mongoose = require('mongoose');
const request = require('supertest');
const app = require('../app');
const jwt = require('jsonwebtoken');
const pool = require('../helpers/connection');

beforeAll(async () => {
  await pool.query(`delete from sample_user where username in ('usertest1', 'usertest2')`);
});

afterAll(async () => {
  await pool.query(`delete from sample_user where username in ('usertest1', 'usertest2')`);
});

describe('Checking authorization', () => {
  test('should return 401 not authorized', async () => {
    return request(app)
      .get('/user')
      .send({ keyword: '', pageNo: '0', pageSize: '10', orderBy: 'id' })
      .expect('Content-Type', "text/html; charset=utf-8")
      .expect(401);
  });
});

describe('POST /auth/register', () => {
  it('should register new user', async () => {
    return request(app)
      .post('/auth/register')
      .send({ username: 'usertest1', password: 'password', email: 'usertest1@gmail.com' })
      .expect('Content-Type', /json/)
      .expect(201)
  });
  it('should not register existing user', async () => {
    return request(app)
      .post('/auth/register')
      .send({ username: 'usertest1', password: 'password', email: 'usertest1@gmail.com' })
      .expect('Content-Type', /json/)
      .expect(400)
  });
});

describe('POST /auth/login', () => {
  it('should failed login with wrong payload', async () => {
    return request(app)
      .post('/auth/login')
      .send({ usernamee: 'usertest2', password: 'password' })
      .expect('Content-Type', /json/)
      .expect(500)
  });
  it('should failed login with nonexistent user', async () => {
    return request(app)
      .post('/auth/login')
      .send({ username: 'usertest2', password: 'password' })
      .expect('Content-Type', /json/)
      .expect(400)
  });
  it('should failed login wrong password', async () => {
    return request(app)
      .post('/auth/login')
      .send({ username: 'usertest1', password: 'password1' })
      .expect('Content-Type', /json/)
      .expect(400)
  });
  it('should successfully login with username', async () => {
    return request(app)
      .post('/auth/login')
      .send({ username: 'usertest1', password: 'password' })
      .expect('Content-Type', /json/)
      .expect(200)
  });
  it('should successfully login with email', async () => {
    return request(app)
      .post('/auth/login')
      .send({ username: 'usertest1@gmail.com', password: 'password' })
      .expect('Content-Type', /json/)
      .expect(200)
  });
});

const token = jwt.sign({ id: 1, username: 'usertest1' }, process.env.TOKEN_SECRET, { expiresIn: '1800s' });

describe('GET /user', () => {
  it('should failed with wrong parameters', async () => {
    return request(app)
      .get('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({ keyword: '', pageNo: 0, pageSize: 10, orderBy: 'id' })
      .expect('Content-Type', /json/)
      .expect(500)
  });
  it('should return all users', async () => {
    return request(app)
      .get('/user?keyword=&pageNo=0&pageSize=10&orderBy=id')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
  });
});

describe('POST /user', () => {
  it('should failed create user with no email', async () => {
    return request(app)
      .post('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({ username: 'usertest2', password: 'password' })
      .expect('Content-Type', /json/)
      .expect(500)
  });
  it('should failed create existing user', async () => {
    return request(app)
      .post('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({ username: 'usertest1', password: 'password', email: 'usertest1@gmail.com' })
      .expect('Content-Type', /json/)
      .expect(400)
  });
  it('should successfully create new user', async () => {
    return request(app)
      .post('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({ username: 'usertest2', password: 'password', email: 'usertest2@gmail.com' })
      .expect('Content-Type', /json/)
      .expect(201)
  });
});

describe('DELETE /user', () => {
  it('should failed remove user with wrong payload', async () => {
    return request(app)
      .delete('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({ usernamee: 'usertest3', password: 'password' })
      .expect('Content-Type', /json/)
      .expect(500)
  });
  it('should failed remove nonexistent user', async () => {
    return request(app)
      .delete('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({ username: 'usertest3', password: 'password' })
      .expect('Content-Type', /json/)
      .expect(400)
  });
  it('should failed remove user with wrong password', async () => {
    return request(app)
      .delete('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({ username: 'usertest2', password: 'password1' })
      .expect('Content-Type', /json/)
      .expect(400)
  });
  it('should successfully remove user', async () => {
    return request(app)
    .delete('/user')
    .set('Authorization', `Bearer ${token}`)
      .send({ username: 'usertest2', password: 'password' })
      .expect('Content-Type', /json/)
      .expect(200)
  });
});