'use strict';

const bcrypt = require('bcryptjs');
const pool = require('../../helpers/connection');

exports.listUser = async (req, res) => {
  try {
    const { keyword, pageNo, pageSize, orderBy } = req.query;
    const users = await pool.query(`SELECT * FROM sample_user
      WHERE (LOWER(username) like '%${keyword.toLowerCase()}%'
      OR LOWER(email) like '%${keyword.toLowerCase()}%')
      ORDER BY ${orderBy}
      LIMIT ${pageSize}
      OFFSET ${pageNo}`);
    console.log(users);
    const noPassword = users.rows.map(({password, ...attribute}) => attribute);
    return res.status(200).json({
      success: true,
      message: 'Got all users successfully.',
      data: noPassword,
    });
  } catch (error) {
    return res.status(500).send({
      success: false,
      message: error
    });
  }
};

// Add User with params in body: username, password, email
exports.addUser = async (req, res) => {
  try {
    const { username, password, email } = req.body;
    const { rows: [userExist] } = await pool.query(`SELECT 1 from sample_user WHERE LOWER(username) = '${username.toLowerCase()}'`);
    if (userExist) {
      return res.status(400).send({
        success: false,
        message: 'User already exist!'
      });
    }
    const hashedPassword = bcrypt.hashSync(password, 10);
    await pool.query(`INSERT INTO sample_user (username, password, email, created_at, last_updated_at, is_active) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
      [
        username,
        hashedPassword,
        email,
        new Date(),
        new Date(),
        true
      ]);
    return res.status(201).send({
      success: true,
      message: 'Success Create User.'
    });
  } catch (error) {
    return res.status(500).send({
      success: false,
      message: error
    });
  }
};

// Remove user with params in body: username, password
exports.removeUser = async (req, res) => {
  try {
    const { username, password } = req.body;
    const { rows: [user] } = await pool.query(`SELECT * FROM sample_user WHERE LOWER(username) = '${username.toLowerCase()}' AND is_active IS TRUE`);
    if (!user) {
      return res.status(400).json({
        success: false,
        error: 'Invalid username!',
      });
    }
    if (user && bcrypt.compareSync(password, user.password)) {
      await pool.query(`UPDATE sample_user SET is_active = $1, last_updated_at = $2 WHERE LOWER(username) = '${username.toLowerCase()}'`, [
        false,
        new Date()
      ]);

      return res.status(200).send({
        success: true,
        message: 'User deleted successfully.'
      });
    } else {
      return res
        .status(400)
        .json({ success: false, error: 'Invalid password!' });
    }
  } catch (error) {
    return res.status(500).send({
      success: false,
      message: error
    });
  }
};