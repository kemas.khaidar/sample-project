'use strict';

const pool = require('../../helpers/connection');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

// Login implementation with params in body: username, password
// It will check username first, if not exist check email with username param
// It will return token for authentication in requests
exports.login = async (req, res) => {
  try {
    const { username, password } = req.body;
    var { rows: [user] } = await pool.query(`SELECT * FROM sample_user WHERE LOWER(username) = '${username.toLowerCase()}' AND is_active IS TRUE`);
    if (!user) {
      const { rows: [userWithEmail] } = await pool.query(`SELECT * FROM sample_user WHERE LOWER(email) = '${username.toLowerCase()}' AND is_active IS TRUE`);
      if (!userWithEmail) {
        return res.status(400).json({
          success: false,
          error: 'Invalid username or email!',
        });
      }
      user = userWithEmail;
    }
    if (user && bcrypt.compareSync(password, user.password)) {
      await pool.query(`UPDATE sample_user SET last_login = $1 WHERE LOWER(username) = '${username.toLowerCase()}'`, [new Date()]);
      const token = jwt.sign({ id: user.id, username: user.username }, process.env.TOKEN_SECRET, { expiresIn: '1800s' });

      return res.status(200).send({
        success: true,
        message: 'Signed in successfully.',
        data: {
          id: user.id,
          username: user.username,
          email: user.email
        },
        access_token: token,
      });
    } else {
      return res
        .status(400)
        .json({ success: false, error: 'Invalid password!' });
    }
  } catch (error) {
    return res.status(500).send({
      success: false,
      message: error
    });
  }
};